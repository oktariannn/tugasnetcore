﻿namespace TugasDotNetCore1.Model
{
    public class TaskModel
    {
        public int? pk_tasks_id { get; set; }
        public string? task_detail { get; set; }
        public int fk_users_id { get; set; }
    }
}
